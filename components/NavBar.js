//import nextJS Link component for client-side navigation
import Link from 'next/link'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { motion } from "framer-motion";

export default function NavBar() {

    return (
        <motion.div exit={{opacity: 0}}>
            <header id="home" className="header">
                <div className="navbar fixed-top">
                    <Link href="/">
                        <a className="navbar-brand navbar-brand-img">
                            <img className="landing-logo" src="/navbar-brand.png"/>
                        </a>
                    </Link>
                    <div className="ml-auto text-right container-fluid text">
                        <nav>
                            <ul className="navbar-nav">
                                <Link href="/work">
                                <a className="nav-link" role="button">Work</a>
                                </Link>
                                <Link href="/about">
                                    <a className="nav-link" role="button">About</a>
                                </Link>
                                <Link href="/contact">
                                    <a className="nav-link" role="button">Contact</a>
                                </Link>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        </motion.div>
    )
}
