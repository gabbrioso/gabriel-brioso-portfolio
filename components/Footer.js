import "react-bootstrap"
import Link from 'next/link'

export default function Footer() {
    //consume the UserContext and destructure it to access the user state from the context provider

    return (
        <footer>
            <div>
                <div className="footer-container container-fluid m-0 p-0">	
                    <div className="bg-light justify-content-between">
                        <div className="footer py-3">

                            &copy; 2020 Copyright: Gab Brioso <b><b>|</b></b> Web Design and Code by Gabriel Brioso <b><b>|</b></b> <Link href="/privacypolicy"><a className="privacy-policy-link text-dark d-inline">Privacy Policy</a></Link>
                                                
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

