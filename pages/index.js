import Head from 'next/head'
import "react-bootstrap"
import { motion } from "framer-motion";

export default function Home() {
  return (
    <motion.div 
      initial={{opacity: 0}}
      animate={{opacity: 1}}
      exit={{opacity: 0}}
      transition={{ duration: 1.5 }}
    >
      <Head>
        <title>Gab Brioso</title>
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:title" content="Gab Brioso"/>
        <meta property="og:description" content="This is Gab Brioso's portfolio that showcases his design works."/>
        <meta property="og:image" content="https://gitlab.com/gabbrioso/gabriel-brioso-portfolio/-/raw/master/public/sitebgx.gif"/>
        <meta property="og:url" content="https://gabriel-brioso-portfolio.vercel.app/work"/>
        <meta name="twitter:card" content="summary_large_image"></meta>
      </Head>
      <div className="landing-container">
        <div className="landing-image">
            <h1 className="landing-text text-center text-lg-left">Hi! I'm Gab Brioso, designer and developer. <br/><br/>I think about design as a discipline<br/>that transcends different modes of practice.</h1>
        </div>
		  </div>
    </motion.div>
  )
}
