import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import Link from 'next/link'
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebookF, faInstagram, faLinkedin, faGitlab, faBehanceSquare } from '@fortawesome/free-brands-svg-icons'


export default function Contact() {
  return (
    <motion.div className="contact-section"
        initial={{opacity: 0}}
        animate={{opacity: 1}}
        exit={{opacity: 0}}
        transition={{ duration: 2 }}
    >
      <Head>
        <title>Gab Brioso - About</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
		<section>
			<motion.div className="container"
                initial={{opacity: 0}}
                animate={{opacity: 1}}
                exit={{opacity: 0}}
                transition={{ duration: 2 }}
            >
				<div className="row">
					<div className="col-lg-12 col-12 contact-text">
                        <h3 className="text-lg-center text-center contact-lead-text"> You can reach me here! </h3>
                        <div className="row container px-5 mx-0 icon-container">
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="mailto:gabbrioso@gmail.com">
                                    <FontAwesomeIcon icon={faEnvelope} />
                                </a>
                            </div>
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="https://www.linkedin.com/in/gab-brioso/">
                                    <FontAwesomeIcon icon={faLinkedin} />
                                </a>
                            </div>
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="https://www.instagram.com/gabbriosoarch/">
                                    <FontAwesomeIcon icon={faInstagram} />
                                </a>
                            </div>
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="https://www.facebook.com/GabBrioso/">
                                    <FontAwesomeIcon icon={faFacebookF} />
                                </a>
                            </div>
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="https://gitlab.com/gabbrioso">
                                    <FontAwesomeIcon icon={faGitlab} />
                                </a>
                            </div>
                            <div className="contact-icons align-items-center col-6 col-lg">
                                <a href="https://www.behance.net/gabrielbrioso">
                                    <FontAwesomeIcon icon={faBehanceSquare} />
                                </a>
                            </div>
                        </div>
					</div>
				</div>
			</motion.div>
		</section>
    </motion.div>
  )
}
