import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export default function Object2() {

    const controls = useAnimation();
    const [ref, inView] = useInView();

    useEffect(() => {
        if (inView) {
        controls.start("visible");
        }
    }, [controls, inView]);

  return (
    <div>
      <Head>
        <title>Gab Brioso - Desk Urbanism</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="work-sub-page">
        <motion.div
            ref={ref}
            animate={controls}
            initial="hidden"
            variants={{
            visible: { opacity: 1 },
            hidden: { opacity: 0 }
            }}
            transition={{ duration: 2 }}
        >
            <section className="work-sub-leadtext">
                <h3 className="gd-text text-right text-light">Desk Urbanism</h3>
                <p className="gd-desc text-right text-light">An exploration on how the form of desk objects affect how you work.</p>
            </section>

                <section className="pt-5 mt-3">
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2A-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2B-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2C-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2D-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2E-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od2F-img.jpeg"/>
                        </div>
                    </div>
                </section>
            </motion.div>
        </div>
    </div>
  )
}
