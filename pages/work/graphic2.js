import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export default function Graphic2() {

    const controls = useAnimation();
    const [ref, inView] = useInView();

    useEffect(() => {
        if (inView) {
        controls.start("visible");
        }
    }, [controls, inView]);

  return (
    <div>
      <Head>
        <title>Gab Brioso - LEX TE MO ON</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="work-sub-page">
        <motion.div
            ref={ref}
            animate={controls}
            initial="hidden"
            variants={{
            visible: { opacity: 1 },
            hidden: { opacity: 0 }
            }}
            transition={{ duration: 2 }}
        >
            <section className="work-sub-leadtext">
                <h3 className="gd-text text-right text-light">LEX TE MO ON</h3>
                <p className="gd-desc text-right text-light">A zine that interospectively dissects Cirilio Bautista's literature. <br/> The work was exhibited by the Center for Campus Art of De La Salle - College of St. Benilde last September 2020.</p>
            </section>

                <section className="pt-5 mt-3">
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2A-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2B-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2C-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2D-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2E-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2F-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2G-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2H-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2I-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2J-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2K-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2L-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2M-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/gd2N-img.jpeg"/>
                        </div>
                    </div>
                </section>
            </motion.div>
        </div>
    </div>
  )
}
