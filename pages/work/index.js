import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import Link from 'next/link'
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import Scrollchor from 'react-scrollchor';

export default function Work() {
  // On Scroll Web Design Section
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  // On Scroll Graphic Design Section
  const controls2 = useAnimation();
  const [ref2, inView2] = useInView();

  useEffect(() => {
    if (inView2) {
      controls2.start("visible");
    }
  }, [controls2, inView2]);
  
  // On Scroll Object Design Section
  const controls3 = useAnimation();
  const [ref3, inView3] = useInView();

  useEffect(() => {
    if (inView3) {
      controls3.start("visible");
    }
  }, [controls3, inView3]);

  // On Scroll Architecture Section
  const controls4 = useAnimation();
  const [ref4, inView4] = useInView();

  useEffect(() => {
    if (inView4) {
      controls4.start("visible");
    }
  }, [controls4, inView4]);

  return (
    <motion.div 
        initial={{opacity: 0}}
        animate={{opacity: 1}}
        exit={{opacity: 0}}
        transition={{ duration: 1.5 }}
    >
      <Head>
        <title>Gab Brioso - Work</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="work-container justify-content-center">
        <div className="work-image">
          <div className="row work-row p-0 m-0 ">
            <div className="col-lg-3 col-12 work-col">
              <Scrollchor className="work-lead-text text-center" to="#webdesign" animate={{offset: 0, duration: 1000}}>Web Design</Scrollchor>
            </div>
            <div className="col-lg-3 col-12 work-col">
              <Scrollchor className="work-lead-text text-center" to="#graphicdesign" animate={{offset: 0, duration: 1000}}>Graphic Design</Scrollchor>
            </div>
            <div className="col-lg-3 col-12 work-col">
              <Scrollchor className="work-lead-text text-center" to="#objectdesign" animate={{offset: 0, duration: 1000}}>Object Design</Scrollchor>
            </div>
            <div className="col-lg-3 col-12 work-col">
              <Scrollchor className="work-lead-text text-center" to="#architecture" animate={{offset: 0, duration: 1000}}>Architecture</Scrollchor>
            </div>     
          </div>
        </div>
		  </section>

      <section id="webdesign">
        <div className="px-4 my-4 text-light">
          <h1 className="font-weight-light font-smaller text-lg-right text-center mt-5">Web Design</h1>
        </div>	
        <motion.div  className="row hoverable"
          ref={ref}
          animate={controls}
          initial="hidden"
          variants={{
            visible: { opacity: 1, x: 0 },
            hidden: { opacity: 0, x: 300 }
          }}
          transition={{ duration: 2 }}
        >
          <div className="col-lg-6 col-12 p-0 m-0">
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <a href="https://patternnn-budget-tracker.vercel.app/" target="_blank">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery1a.jpg"/>
                    <span className="gallery-caption">
                      <h4>PATTERNNN<br/>(MERN Stack App)</h4>
                    </span>
                  </div>
                </a>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <a href="https://spotify-clone-2-9bccc.web.app/" target="_blank">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery1b.jpg"/>
                    <span className="gallery-caption">
                      <h4>Spotify Clone<br/>(React App)</h4>
                    </span>
                  </div>
                </a>
              </motion.div>
            </div>
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <a href="https://gabbrioso.gitlab.io/2020-08-08-web_proj_1/" target="_blank">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery1c.jpg"/>
                    <span className="gallery-caption">
                      <h4>Neue Tectonics<br/>(Static App)</h4>
                    </span>
                  </div>
                </a>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <a href="https://covid-tracker-2.vercel.app/" target="_blank">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery1d.jpg"/>
                    <span className="gallery-caption">
                      <h4>Covid Tracker<br/>(NextJS App<br/>Rapid API)</h4>
                    </span>
                  </div>
                </a>
              </motion.div>
            </div>	
          </div>

          <motion.div className="col-lg-6 col-12 p-0 m-0 gallery-img2"
            whileHover={{ scale: 0.95 }}
            onHoverStart={e => {}}
            onHoverEnd={e => {}}
          >
            <a href="https://gabbrioso.gitlab.io/the-authenticity-zero-collective-2/" target="_blank">
              <div className="gallery-img-wrap">
                <img className="align-center" src="./gallery1e.jpg"/>
                <span className="gallery-caption">
                  <h4>The Authenticity Zero Collective<br/>(MEN Stack App)</h4>
                </span>
              </div>
            </a>
          </motion.div>
			  </motion.div>
      </section>

      <section id="graphicdesign">
        <div className="px-4 my-4 text-light">
          <h1 className="font-weight-light font-smaller text-lg-right text-center mt-5">Graphic Design</h1>
        </div>	
        <motion.div className="row hoverable"
          ref={ref2}
          animate={controls2}
          initial="hidden"
          variants={{
            visible: { opacity: 1, x: 0 },
            hidden: { opacity: 0, x: -300 }
          }}
          transition={{ duration: 2 }}
        >
          <div className="col-lg-6 col-12 p-0 m-0">
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/graphic1">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery2a.jpg"/>
                    <span className="gallery-caption">
                      <h4>Woba</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/graphic2">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery2b.jpg"/>
                    <span className="gallery-caption">
                      <h4>lex te mo on</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/graphic3">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery2c.jpg"/>
                    <span className="gallery-caption">
                      <h4>Anabolism</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/graphic4">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery2d.jpg"/>
                    <span className="gallery-caption">
                      <h4>Random Form</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>	
          </div>

          <motion.div className="col-lg-6 col-12 p-0 m-0 gallery-img2"
            whileHover={{ scale: 0.95 }}
            onHoverStart={e => {}}
            onHoverEnd={e => {}}
          >
            <Link href="/work/graphic5">
              <div className="gallery-img-wrap">
                <img className="align-center" src="./gallery2e.jpg"/>
                <span className="gallery-caption">
                  <h4>Postcard Drawings</h4>
                </span>
              </div>
            </Link>
          </motion.div>
			  </motion.div>
      </section>

      <section id="objectdesign">
        <div className="px-4 my-4 text-light">
          <h1 className="font-weight-light font-smaller text-lg-right text-center mt-5">Object Design</h1>
        </div>	
        <motion.div className="row hoverable"
          ref={ref3}
          animate={controls3}
          initial="hidden"
          variants={{
            visible: { opacity: 1, x: 0},
            hidden: { opacity: 0, x: 300 }
          }}
          transition={{ duration: 2 }}
        >
          <div className="col-lg-6 col-12 p-0 m-0">
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/object1">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery3a.jpg"/>
                    <span className="gallery-caption">
                      <h4>Love-in-<br/>Asphyxiation</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/object2">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery3b.jpg"/>
                    <span className="gallery-caption">
                      <h4>Desk Urbanism</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/object3">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery3c.jpg"/>
                    <span className="gallery-caption">
                      <h4>Table House</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/object4">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery3d.jpg"/>
                    <span className="gallery-caption">
                      <h4>Mech Desk</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>	
          </div>

          <motion.div className="col-lg-6 col-12 p-0 m-0 gallery-img2"
            whileHover={{ scale: 0.95 }}
            onHoverStart={e => {}}
            onHoverEnd={e => {}}
          >
            <Link href="/work/object5">
              <div className="gallery-img-wrap">
                <img className="align-center" src="./gallery3e.jpg"/>
                <span className="gallery-caption">
                  <h4>Singularities & Multiplicities</h4>
                </span>
              </div>
            </Link>
          </motion.div>
			  </motion.div>
      </section>

      <section id="architecture">
        <div className="px-4 my-4 text-light">
          <h1 className="font-weight-light font-smaller text-lg-right text-center mt-5">Architecture</h1>
        </div>	
        <motion.div  className="row hoverable mb-5"
          ref={ref4}
          animate={controls4}
          initial="hidden"
          exit="visible"
          variants={{
            visible: { opacity: 1, x: 0 },
            hidden: { opacity: 0, x: -300 }
          }}
          transition={{ duration: 2 }}
        >
          <div className="col-lg-6 col-12 p-0 m-0">
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/architecture1">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery4a.jpg"/>
                    <span className="gallery-caption">
                      <h4>Circle of Influence</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/architecture2">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery4b.jpg"/>
                    <span className="gallery-caption">
                      <h4>Canvas House</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>
            <div className="row p-0 m-0">
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/architecture3">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery4c.jpg"/>
                    <span className="gallery-caption">
                      <h4>Open Private</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
              <motion.div className="col p-0 m-0 gallery-img"
                whileHover={{ scale: 0.9 }}
                onHoverStart={e => {}}
                onHoverEnd={e => {}}
              >
                <Link href="/work/architecture4">
                  <div className="gallery-img-wrap">
                    <img className="align-center" src="./gallery4d.jpg"/>
                    <span className="gallery-caption">
                      <h4>Poetry Architecture</h4>
                    </span>
                  </div>
                </Link>
              </motion.div>
            </div>	
          </div>

          <motion.div className="col-lg-6 col-12 p-0 m-0 gallery-img2"
            whileHover={{ scale: 0.95 }}
            onHoverStart={e => {}}
            onHoverEnd={e => {}}
          >
            <Link href="/work/architecture5">
              <div className="gallery-img-wrap">
                <img className="align-center" src="./gallery4e.jpg"/>
                <span className="gallery-caption">
                  <h4>Bauhaus Neue</h4>
                </span>
              </div>
            </Link>
          </motion.div>
			  </motion.div>
      </section>
    </motion.div>
  )
}
