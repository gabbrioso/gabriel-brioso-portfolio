import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export default function Object1() {

    const controls = useAnimation();
    const [ref, inView] = useInView();

    useEffect(() => {
        if (inView) {
        controls.start("visible");
        }
    }, [controls, inView]);

  return (
    <div>
      <Head>
        <title>Gab Brioso - Love-in-Asphyxiation</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="work-sub-page">
        <motion.div
            ref={ref}
            animate={controls}
            initial="hidden"
            variants={{
            visible: { opacity: 1 },
            hidden: { opacity: 0 }
            }}
            transition={{ duration: 2 }}
        >
            <section className="work-sub-leadtext">
                <h3 className="gd-text text-right text-light">Love-in-Asphyxiation</h3>
                <p className="gd-desc text-right text-light">An analysis of the cybernetic systems of human relations through the lens of Shakespeare's Midsummernight's Dream.<br/> The work was exhibited in the Cultural Center of the Philippines last October 2015 <br/> and in the Center for Campus Art of the De La Salle - College of St.Benilde last February 2016.</p>
            </section>

                <section className="pt-5 mt-3">
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od1A-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od1B-img.jpeg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/od1C-img.jpeg"/>
                        </div>
                    </div>
                </section>
            </motion.div>
        </div>
    </div>
  )
}
