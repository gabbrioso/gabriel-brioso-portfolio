import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export default function Architecture4() {

    const controls = useAnimation();
    const [ref, inView] = useInView();

    useEffect(() => {
        if (inView) {
        controls.start("visible");
        }
    }, [controls, inView]);

  return (
    <div>
      <Head>
        <title>Gab Brioso - Into the Future</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="work-sub-page">
        <motion.div
            ref={ref}
            animate={controls}
            initial="hidden"
            variants={{
            visible: { opacity: 1 },
            hidden: { opacity: 0 }
            }}
            transition={{ duration: 2 }}
        >
            <section className="work-sub-leadtext">
                <h3 className="gd-text text-right text-light">Poetry Architecture</h3>
                <p className="gd-desc text-right text-light">A translation of Charles Boris Manez's poetry into visual form.</p>
            </section>

                <section className="pt-5 mt-3">
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4A-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4B-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4C-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4D-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4E-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4F-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4G-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4H-img.jpg"/>
                        </div>
                    </div>
                    <div className="row px-3">
                        <div className="img-container col-12">
                            <img className="img-fluid" src="/a4I-img.jpg"/>
                        </div>
                    </div>
                </section>
            </motion.div>
        </div>
    </div>
  )
}
