import { AnimatePresence } from "framer-motion";
import '../styles/globals.css'
import Navbar from '../components/NavBar'
import Footer from '../components/Footer'
import CustomCursor from '../components/CustomCursor'
import "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
  return (
    <AnimatePresence initial={false} exitBeforeEnter>
      <CustomCursor />
      <Navbar />
        <Component {...pageProps} />
      <Footer />
    </AnimatePresence>
  )
}

export default MyApp
