import "react-bootstrap"
import Head from 'next/head'

export default function PrivacyPolicy() {
    //consume the UserContext and destructure it to access the user state from the context provider

    return (
        <>
            <Head>
                <title>PATTERNNN - Privacy Policy</title>
            </Head>
            <section class="privacy-backdrop">
                <div class="landing-container">
                    <div class="container privacy-backdrop pt-5">
                        <h1 class="privacy-lead-text py-5 font-weight-light text-light text-center">Privacy Policy</h1>
                        <p class="text-center text-light py-5 m-0">
                        WHO I AM<br/><br/>
                        I am Gabriel Brioso.<br/><br/>

                        WHAT PERSONAL DATA WE COLLECT AND WHY WE COLLECT IT<br/><br/>
                        COMMENTS<br/><br/>
                        When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and br/owser user agent string to help spam detection.<br/><br/>

                        An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.<br/><br/>

                        MEDIA<br/><br/>
                        If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.<br/><br/>

                        CONTACT FORMS<br/><br/>
                        COOKIES<br/><br/>
                        If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.<br/><br/>

                        If you visit our login page, we will set a temporary cookie to determine if your br/owser accepts cookies. This cookie contains no personal data and is discarded when you close your br/owser.<br/><br/>

                        When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select "Remember Me", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.<br/><br/>

                        If you edit or publish an article, an additional cookie will be saved in your br/owser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.<br/><br/>

                        EMBEDDED CONTENT FROM OTHER WEBSITES<br/><br/>
                        Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.<br/><br/>

                        These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.<br/><br/>

                        ANALYTICS<br/><br/>
                        WHO WE SHARE YOUR DATA WITH<br/><br/>
                        HOW LONG WE RETAIN YOUR DATA<br/><br/>
                        If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.<br/><br/>

                        For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.<br/><br/>

                        WHAT RIGHTS YOU HAVE OVER YOUR DATA<br/><br/>
                        If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.<br/><br/>

                        WHERE WE SEND YOUR DATA<br/><br/>
                        Visitor comments may be checked through an automated spam detection service.<br/><br/>

                        YOUR CONTACT INFORMATION<br/><br/>
                        ADDITIONAL INFORMATION<br/><br/>
                        HOW WE PROTECT YOUR DATA<br/><br/>
                        WHAT DATA BR/EACH PROCEDURES WE HAVE IN PLACE<br/><br/>
                        WHAT THIRD PARTIES WE RECEIVE DATA FROM<br/><br/>
                        WHAT AUTOMATED DECISION MAKING AND/OR PROFILING WE DO WITH USER DATA<br/><br/>
                        INDUSTRY REGULATORY DISCLOSURE REQUIREMENTS</p>
                    </div>
                </div>
            </section>
        </>
    )
}

