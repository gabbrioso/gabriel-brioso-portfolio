import Head from 'next/head'
import "react-bootstrap"
import { motion, useAnimation } from "framer-motion";
import Link from 'next/link'
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";

export default function About() {
	// On Scroll About Me Section
	const controls = useAnimation();
	const [ref, inView] = useInView();
  
	useEffect(() => {
	  if (inView) {
		controls.start("visible");
	  }
	}, [controls, inView]);

	// On Scroll Skills Section
	const controls2 = useAnimation();
	const [ref2, inView2] = useInView();
  
	useEffect(() => {
	  if (inView2) {
		controls2.start("visible");
	  }
	}, [controls2, inView2]);

  return (
    <motion.div className="about-section"
        initial={{opacity: 0}}
        animate={{opacity: 1}}
        exit={{opacity: 0}}
        transition={{ duration: 2 }}
    >
      <Head>
        <title>Gab Brioso - About</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <section className="about-container">
			<div className="about-image">
				<h1 className="about-lead-text">Gab Brioso</h1>
				<h3 className="about-text">Web Developer | UI Designer | Artist | Architect</h3>
			</div>
		</section>

		<section>
			<div className="container">
				<motion.div className="row"
					ref={ref}
					animate={controls}
					initial="hidden"
					variants={{
						visible: { opacity: 1, x: 0 },
						hidden: { opacity: 0, x: 300 }
					}}
					transition={{ duration: 2 }}
				>
					<div className="col-lg-6 col-12">
						<img className="about-sub-img img-fluid py-lg-5 my-lg-5 py-3 my-3" src="./about-img.jpg"/>						
					</div>
					<div className="col-lg-6 col-12">
						<p className="about-text-desc py-lg-5 my-lg-5 px-5 mx-lg-5 text-lg-left text-center">Gab Brioso is a web developer, a UI/UX Designer and an architect that thinks about design <b>holistically</b>. Trained and educated in the discipline of architecture, he understands the technical and creative implications of a product; be it a website, an object, an artpiece or even to the scale of a space. <br/><br/>He takes interest in the intersection of digital and analog processes. His passion to enhance people's experience of a design product, both digitally and physically, is what fuels his work.</p>
					</div>
				</motion.div>
			</div>
		</section>

		<hr/>

		<section>
			<div className="container">
				<div className="text-center my-5 mx-3">
					<h4 className="about-sub-text" >He is knowledgable in an array of digital tools.</h4>
				</div>				
				<motion.div className="row"
					ref={ref2}
					animate={controls2}
					initial="hidden"
					variants={{
						visible: { opacity: 1, x: 0 },
						hidden: { opacity: 0, x: -300 }
					}}
					transition={{ duration: 2 }}
				>
					<div className="container-fluid col-lg-3 col-12">
						<div className="about-icon">
							<img className="img-fluid" src="./about-icon1.jpeg"/>
						</div>
						<h6 className="text-center">Web Development | UI/UX Design</h6>
						<div className="text-center">
							<p>
								<b>+</b> HTML5<br/>
								<b>+</b> <b>+</b>CSS3<br/>
								<b>+</b> Bootstrap<br/>
								<b>+</b> Git/Gitlab<br/>
								<b>+</b> JavaScript<br/>
								<b>+</b> React<br/>
								<b>+</b> Next.js<br/>
								<b>+</b> MongoDB<br/>
								<b>+</b> Express<br/>	
								<b>+</b> NodeJS<br/>
							</p>
						</div>
					</div>

					<div className="container-fluid col-lg-3 col-12">
						<div className="about-icon">
							<img className="img-fluid" src="./about-icon2.jpeg"/>
						</div>
						<h6 className="text-center">Graphics and Layout</h6>
						<div className="text-center">
							<p>
								<b>+</b> Photoshop<br/>
								<b>+</b> Illustrator<br/>
								<b>+</b> After Effects<br/>
								<b>+</b> Premiere<br/>
								<b>+</b> Adobe UX<br/>
							</p>
						</div>
					</div>
					<div className="container-fluid col-lg-3 col-12 mb-3">
						<div className="about-icon">
							<img className="img-fluid" src="./about-icon3.jpeg"/>
						</div>
						<h6 className="text-center">3D Modelling</h6>
						<div className="text-center">
							<p>
								<b>+</b> Rhino 3D<br/>
								<b>+</b> 3DS Max<br/>
								<b>+</b> Cinema4D<br/>
								<b>+</b> SketchUp<br/>
								<b>+</b> Lumion<br/>
							</p>
						</div>
					</div>
				</motion.div>
			</div>
		</section>

		<br/>
		<hr/>
		<br/>
		
		<section>
			<div className="container py-3">
				<div className="text-center">
					<h4>Let's collaborate!</h4>
					<Link href="/contact">
					 	<a className="btn btn-outline-dark my-4" href="./contact.html">Contact</a>
					</Link>
				</div>
			</div>
		</section>
    </motion.div>
  )
}
